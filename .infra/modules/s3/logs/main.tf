variable "bucketId" {}
variable "tags" {}

locals {
  bucketName = "${var.tags["Application"]}-${terraform.workspace}-${var.bucketId}-logs"
}
