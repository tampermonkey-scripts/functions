variable "bucketId" {}
variable "tags" {}

locals {
  bucketName = "${var.tags["Application"]}-${terraform.workspace}-${var.bucketId}-public"
}

resource "aws_s3_bucket" "public" {

  bucket = local.bucketName
  acl    = "public-read"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PublicReadGetObject",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${local.bucketName}/*"
    }
  ]
}
EOF

  website {
    index_document = "index.html"
    # error_document = "error.html"

    routing_rules = <<EOF
[{
  "Condition": {
    "KeyPrefixEquals": "docs/"
  },
  "Redirect": {
    "ReplaceKeyPrefixWith": "documents/"
  }
}]
EOF
  }
}
