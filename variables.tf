variable "tags" {
  description = "Tags to use on all created resources"
  type = map
}
