declare var grecaptcha: any;
declare var verified: any;

class Coins {

  private get(url: string): Promise<any> {
    return new Promise<any>((resolve: Function, reject: Function) => {
      const request: XMLHttpRequest = new XMLHttpRequest();
      request.onreadystatechange = () => {
        if (request.readyState === 4) {
          resolve(request.response);
        }
        else {
          reject(request.status);
        }
      };
      request.open("GET", url, true);
      request.send();
    });
  }

  private minimum(url: string, currency: string): Promise<Number> {
    const minRegex: RegExp = new RegExp(`<div\\sclass=\"form-hint\">Min\\s-\\s<span\\sclass=\"underline-text\">(.*)\\s${currency}<\\/span><\\/div>`, "g");
    return new Promise<Number>((resolve: Function) => {
      this.get(`${url}/account`).then(response => {
        let m: string[] = minRegex.exec(response);
        resolve(new Number(m[1] || 0));
      });
    });
  }

  private clickClaim(captcha: string) {
    const claimButton: HTMLElement = document.querySelector("#get-free");
    if (claimButton.style.display !== "none") {
      grecaptcha.ready(() => {
        grecaptcha.execute(captcha, {action: 'homepage'}).then(token => {
          claimButton.click();
          verified = token;
        });
      });
    }
  }

  private getBalance(currency: string): Number {
    const balance: HTMLElement = document.querySelector("#balance");
    return new Number(balance.innerHTML.replace(` ${currency}`, ""));
  }

  public claim(url: string, currency: string, captcha: string) {
    const amount: Number = this.getBalance(currency);
    this.minimum(url, currency).then(minimum => {
      if (amount > minimum) {
        window.location.href = `${url}/account`;
      }
      else {
        this.clickClaim(captcha);
      }
    });
  }

  public withdrawal(url: string, currency: string, wallet: string) {

    const withdrawal: HTMLElement = document.querySelector("#form-withdrawal > div > div > span");
    const withdrawalBtn: HTMLElement = document.querySelector("#form-withdrawal div.btn-min.btn-blue");
    const wamount: HTMLInputElement = <HTMLInputElement><unknown>document.querySelector("#wamount");
    const address: HTMLInputElement = <HTMLInputElement><unknown>document.querySelector("input[name=address]");
    const low: HTMLInputElement = <HTMLInputElement><unknown>document.querySelector("input[name=radio]#low");

    const amount: Number = this.getBalance(currency);
    const min: Number = new Number(withdrawal.innerHTML.replace(` ${currency}`, ""));

    if (amount <= min) window.location.href = url;

    wamount.value = <string><unknown>amount;
    address.value = wallet;
    low.checked = true;
    withdrawalBtn.click();

    window.location.href = url;
  }

}
