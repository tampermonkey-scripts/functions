provider "aws" {
  region = "eu-west-3"
}

terraform {
  backend "s3" {
    region = "eu-west-3"
    bucket = "fco-terraform-tfstate"
    workspace_key_prefix = "public-functions"
    key = "terraform.tfstate"
  }
}

resource "random_string" "bucket" {
  length = 10
  special = false
  upper = false
}

module "s3-public" {
  source = "./.infra/modules/s3/public"
  bucketId = random_string.bucket.result
  tags = var.tags
}

module "s3-logs" {
  source = "./.infra/modules/s3/logs"
  bucketId = random_string.bucket.result
  tags = var.tags
}
